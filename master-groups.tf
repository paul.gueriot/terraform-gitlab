module "group" {
  source = "./modules/gitlab-groups"
  groups = {
    "toto" = {
      path                   = "toto"
      project_creation_level = "noone"
      saml_links = {
        "GITLAB_owner" = "owner"
      }
    }
  }
}
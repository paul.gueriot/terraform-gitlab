terraform {
  required_version = "~> 1.3"

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "16.9.1"
    }
    sops = {
      source  = "carlpett/sops"
      version = "1.0.0"
    }
  }
}

provider "gitlab" {
}

provider "sops" {

}

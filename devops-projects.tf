module "devops_projects" {
  source                  = "./modules/gitlab-projects"
  namespace_id            = module.devops_group.groups["devops"].id
  approval_rule_group_ids = [module.group.groups["toto"].id]

  projects = {
    "argo-workflows" = {
      description        = "Argo Workflows"
      push_rules         = {}
      branch_protection  = {}
      approval_rule      = {}
      level_mr_approvals = {}
    }
  }
}

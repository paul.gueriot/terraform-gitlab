module "devops_group" {
  source    = "./modules/gitlab-groups"
  parent_id = module.group.groups["toto"].id
  groups = {
    "devops" = {
      project_creation_level = "maintainer"
    }
  }
}

module "devops_subgroups" {
  source    = "./modules/gitlab-groups"
  parent_id = module.devops_group.groups["devops"].id
  groups = {
    "app"            = {}
  }
}

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.5 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | 16.9.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | 16.9.1 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [gitlab_group.this](https://registry.terraform.io/providers/gitlabhq/gitlab/16.9.1/docs/resources/group) | resource |
| [gitlab_group_saml_link.saml_link](https://registry.terraform.io/providers/gitlabhq/gitlab/16.9.1/docs/resources/group_saml_link) | resource |
| [gitlab_group_share_group.share_group](https://registry.terraform.io/providers/gitlabhq/gitlab/16.9.1/docs/resources/group_share_group) | resource |
| [gitlab_group_variable.variable](https://registry.terraform.io/providers/gitlabhq/gitlab/16.9.1/docs/resources/group_variable) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_groups"></a> [groups](#input\_groups) | Map for list of groups | <pre>map(object({<br>    path                               = optional(string)<br>    auto_devops_enabled                = optional(bool, false)<br>    avatar                             = optional(string, null)<br>    avatar_hash                        = optional(string, null)<br>    default_branch_protection          = optional(number, 3)<br>    description                        = optional(string, null)<br>    emails_disabled                    = optional(bool, false)<br>    extra_shared_runners_minutes_limit = optional(number, null)<br>    ip_restriction_ranges              = optional(list(string), [])<br>    lfs_enabled                        = optional(bool, true)<br>    membership_lock                    = optional(bool, true)<br>    mentions_disabled                  = optional(bool, false)<br>    prevent_forking_outside_group      = optional(bool, false)<br>    project_creation_level             = optional(string, "maintainer")<br>    request_access_enabled             = optional(bool, false)<br>    require_two_factor_authentication  = optional(bool, false)<br>    share_with_group_lock              = optional(bool, true)<br>    shared_runners_minutes_limit       = optional(number, null)<br>    subgroup_creation_level            = optional(string, "owner")<br>    two_factor_grace_period            = optional(number, 48)<br>    visibility_level                   = optional(string, "private")<br>    wiki_access_level                  = optional(string, "disabled")<br>    saml_links                         = optional(map(string), {})<br>    share_groups                       = optional(map(list(number)), {})<br>    variables = optional(map(object({<br>      value             = string<br>      protected         = optional(bool, false)<br>      masked            = optional(bool, false)<br>      environment_scope = optional(string, "*")<br>      variable_type     = optional(string, "env_var")<br>    })), {})<br>  }))</pre> | n/a | yes |
| <a name="input_parent_id"></a> [parent\_id](#input\_parent\_id) | Parent group id | `number` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_groups"></a> [groups](#output\_groups) | List of created groups with module |
<!-- END_TF_DOCS -->
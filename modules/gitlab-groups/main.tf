resource "gitlab_group" "this" {
  for_each = var.groups

  name                               = each.key
  path                               = each.value.path == null ? each.key : each.value.path
  parent_id                          = var.parent_id
  auto_devops_enabled                = each.value.auto_devops_enabled
  avatar                             = each.value.avatar
  avatar_hash                        = each.value.avatar_hash
  default_branch_protection          = each.value.default_branch_protection
  description                        = each.value.description
  extra_shared_runners_minutes_limit = each.value.extra_shared_runners_minutes_limit
  ip_restriction_ranges              = each.value.ip_restriction_ranges
  lfs_enabled                        = each.value.lfs_enabled
  membership_lock                    = each.value.membership_lock
  mentions_disabled                  = each.value.mentions_disabled
  prevent_forking_outside_group      = each.value.prevent_forking_outside_group
  project_creation_level             = each.value.project_creation_level
  request_access_enabled             = each.value.request_access_enabled
  require_two_factor_authentication  = each.value.require_two_factor_authentication
  share_with_group_lock              = each.value.share_with_group_lock
  shared_runners_minutes_limit       = each.value.shared_runners_minutes_limit
  subgroup_creation_level            = each.value.subgroup_creation_level
  two_factor_grace_period            = each.value.two_factor_grace_period
  visibility_level                   = each.value.visibility_level
  wiki_access_level                  = each.value.wiki_access_level
}


locals {
  saml_links = flatten([
    for group_key, group in var.groups : [
      for saml_link_key, access_level in group.saml_links : {
        group_key       = group_key
        access_level    = access_level
        saml_group_name = saml_link_key
      }
    ]
  ])

  share_groups = flatten([
    for group_key, group in var.groups : [
      for access_level, groups_access in group.share_groups : [
        for group_access_id in groups_access : {
          group_key      = group_key
          access_level   = access_level
          share_group_id = group_access_id
        }
      ]
    ]
  ])

  variables = flatten([
    for group_key, group in var.groups : [
      for key, values in group.variables : {
        group_key         = group_key
        key               = key
        value             = values.value
        protected         = values.protected
        masked            = values.masked
        environment_scope = values.environment_scope
        variable_type     = values.variable_type
      }
    ]
  ])
}

resource "gitlab_group_saml_link" "saml_link" {
  for_each = {
    for sl in local.saml_links : "${sl.group_key}.${sl.saml_group_name}" => sl
  }

  group           = gitlab_group.this[each.value.group_key].id
  access_level    = each.value.access_level
  saml_group_name = each.value.saml_group_name

  depends_on = [gitlab_group.this]
}

resource "gitlab_group_share_group" "share_group" {
  for_each = {
    for sg in local.share_groups : "${sg.group_key}.${sg.share_group_id}" => sg
  }
  group_id       = gitlab_group.this[each.value.group_key].id
  share_group_id = each.value.share_group_id
  group_access   = each.value.access_level

  depends_on = [gitlab_group.this]
}

resource "gitlab_group_variable" "variable" {
  for_each = {
    for v in local.variables : "${v.group_key}.${v.key}" => v
  }

  group             = gitlab_group.this[each.value.group_key].id
  key               = each.value.key
  value             = each.value.value
  protected         = each.value.protected
  masked            = each.value.masked
  environment_scope = each.value.environment_scope
}
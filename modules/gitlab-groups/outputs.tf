
output "groups" {
  description = "List of created groups with module"
  value       = gitlab_group.this
}

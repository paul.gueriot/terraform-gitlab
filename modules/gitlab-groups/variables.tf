
variable "groups" {
  description = "Map for list of groups"
  type = map(object({
    path                               = optional(string)
    auto_devops_enabled                = optional(bool, false)
    avatar                             = optional(string, null)
    avatar_hash                        = optional(string, null)
    default_branch_protection          = optional(number, 3)
    description                        = optional(string, null)
    extra_shared_runners_minutes_limit = optional(number, null)
    ip_restriction_ranges              = optional(list(string), [])
    lfs_enabled                        = optional(bool, true)
    membership_lock                    = optional(bool, true)
    mentions_disabled                  = optional(bool, false)
    prevent_forking_outside_group      = optional(bool, false)
    project_creation_level             = optional(string, "maintainer")
    request_access_enabled             = optional(bool, false)
    require_two_factor_authentication  = optional(bool, false)
    share_with_group_lock              = optional(bool, true)
    shared_runners_minutes_limit       = optional(number, null)
    subgroup_creation_level            = optional(string, "owner")
    two_factor_grace_period            = optional(number, 48)
    visibility_level                   = optional(string, "private")
    wiki_access_level                  = optional(string, "disabled")
    saml_links                         = optional(map(string), {})
    share_groups                       = optional(map(list(number)), {})
    variables = optional(map(object({
      value             = string
      protected         = optional(bool, false)
      masked            = optional(bool, false)
      environment_scope = optional(string, "*")
      variable_type     = optional(string, "env_var")
    })), {})
  }))
}


variable "parent_id" {
  description = "Parent group id"
  type        = number
  default     = null
}


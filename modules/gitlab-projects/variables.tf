
variable "projects" {
  description = "Map for list of projects"
  type = map(object({
    allow_merge_on_skipped_pipeline                  = optional(bool, false)
    analytics_access_level                           = optional(string, "enabled")
    approvals_before_merge                           = optional(number, 0)
    archived                                         = optional(bool, false)
    auto_cancel_pending_pipelines                    = optional(string, "enabled")
    auto_devops_deploy_strategy                      = optional(string, null)
    auto_devops_enabled                              = optional(bool, false)
    autoclose_referenced_issues                      = optional(bool, true)
    avatar                                           = optional(string, null)
    build_git_strategy                               = optional(string, "fetch")
    build_timeout                                    = optional(number, 3600)
    builds_access_level                              = optional(string, "enabled")
    ci_config_path                                   = optional(string, null)
    ci_default_git_depth                             = optional(number, 20)
    ci_forward_deployment_enabled                    = optional(bool, true)
    ci_restrict_pipeline_cancellation_role           = optional(string, "developer")
    ci_separated_caches                              = optional(bool, true)
    container_registry_access_level                  = optional(string, "disabled")
    default_branch                                   = optional(string, "master")
    description                                      = optional(string, null)
    environments_access_level                        = optional(string, "disabled")
    external_authorization_classification_label      = optional(string, null)
    feature_flags_access_level                       = optional(string, "disabled")
    forked_from_project_id                           = optional(number, null)
    group_runners_enabled                            = optional(bool, true)
    infrastructure_access_level                      = optional(string, "disabled")
    initialize_with_readme                           = optional(bool, false)
    issues_access_level                              = optional(string, "disabled")
    issues_enabled                                   = optional(bool, false)
    issues_template                                  = optional(string, null)
    keep_latest_artifact                             = optional(bool, true)
    lfs_enabled                                      = optional(bool, true)
    merge_commit_template                            = optional(string, null)
    merge_method                                     = optional(string, "merge")
    merge_pipelines_enabled                          = optional(bool, false)
    merge_requests_access_level                      = optional(string, "enabled")
    merge_requests_enabled                           = optional(bool, true)
    merge_requests_template                          = optional(string, null)
    merge_trains_enabled                             = optional(bool, false)
    monitor_access_level                             = optional(string, "enabled")
    only_allow_merge_if_all_discussions_are_resolved = optional(bool, true)
    only_allow_merge_if_pipeline_succeeds            = optional(bool, true)
    packages_enabled                                 = optional(bool, false)
    pages_access_level                               = optional(string, "disabled")
    path                                             = optional(string, null)
    printing_merge_request_link_enabled              = optional(bool, true)
    releases_access_level                            = optional(string, "enabled")
    remove_source_branch_after_merge                 = optional(bool, true)
    repository_access_level                          = optional(string, "enabled")
    repository_storage                               = optional(string, null)
    request_access_enabled                           = optional(bool, false)
    requirements_access_level                        = optional(string, "disabled")
    resolve_outdated_diff_discussions                = optional(bool, true)
    restrict_user_defined_variables                  = optional(bool, false)
    security_and_compliance_access_level             = optional(string, "private")
    shared_runners_enabled                           = optional(bool, false)
    snippets_access_level                            = optional(string, "enabled")
    snippets_enabled                                 = optional(bool, true)
    squash_commit_template                           = optional(string, null)
    squash_option                                    = optional(string, "default_off")
    suggestion_commit_message                        = optional(string, null)
    tags                                             = optional(list(string), [])
    visibility_level                                 = optional(string, "private")
    wiki_access_level                                = optional(string, "disabled")
    wiki_enabled                                     = optional(bool, false)
    push_rules = object({
      author_email_regex            = optional(string, null)
      branch_name_regex             = optional(string, "(renovate|feature|feat|fix|hotfix|chore|breaking-change|ci|docs|style|refactor|perf|test|release|revert)\\/*")
      commit_committer_check        = optional(bool, false)
      commit_message_negative_regex = optional(string, "")
      commit_message_regex          = optional(string, null)
      deny_delete_tag               = optional(bool, false)
      file_name_regex               = optional(string, null)
      max_file_size                 = optional(number, 0)
      member_check                  = optional(bool, true)
      prevent_secrets               = optional(bool, true)
      reject_unsigned_commits       = optional(bool, false)
    })
    branch_protection = object({
      enabled                      = optional(bool, true)
      allow_force_push             = optional(bool, false)
      push_access_level            = optional(string, "no one")
      merge_access_level           = optional(string, "developer")
      code_owner_approval_required = optional(bool, true)
      users_allowed_to_push        = optional(list(number), [])
    })
    approval_rule = object({
      enabled            = optional(bool, true)
      name               = optional(string, "Review rule")
      approvals_required = optional(number, 1)
    })
    level_mr_approvals = object({
      enabled                                        = optional(bool, true)
      reset_approvals_on_push                        = optional(bool, false)
      disable_overriding_approvers_per_merge_request = optional(bool, false)
      merge_requests_author_approval                 = optional(bool, false)
      merge_requests_disable_committers_approval     = optional(bool, false)
    })
    variables = optional(map(object({
      value             = string
      protected         = optional(bool, false)
      masked            = optional(bool, false)
      environment_scope = optional(string, "*")
      variable_type     = optional(string, "env_var")
    })), {})
    schedules = optional(map(object({
      ref           = string
      cron          = string
      active        = bool
      cron_timezone = optional(string, "Europe/Paris")
      variables = optional(map(object({
        value = string
      })), {})
    })), {})
  }))
}

variable "namespace_id" {
  description = "Namespace id"
  type        = number
}

variable "approval_rule_group_ids" {
  description = "List of group ids for approval rule"
  type        = list(number)
  default     = []
}

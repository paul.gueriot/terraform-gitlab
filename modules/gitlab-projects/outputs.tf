
output "projects" {
  description = "List of created projects with module"
  value       = gitlab_project.this
}

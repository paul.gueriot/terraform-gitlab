resource "gitlab_project" "this" {
  for_each = var.projects

  name                                   = each.key
  allow_merge_on_skipped_pipeline        = each.value.allow_merge_on_skipped_pipeline
  analytics_access_level                 = each.value.analytics_access_level
  approvals_before_merge                 = each.value.approvals_before_merge
  archived                               = each.value.archived
  auto_cancel_pending_pipelines          = each.value.auto_cancel_pending_pipelines
  auto_devops_deploy_strategy            = each.value.auto_devops_deploy_strategy
  auto_devops_enabled                    = each.value.auto_devops_enabled
  autoclose_referenced_issues            = each.value.autoclose_referenced_issues
  avatar                                 = each.value.avatar
  build_git_strategy                     = each.value.build_git_strategy
  build_timeout                          = each.value.build_timeout
  builds_access_level                    = each.value.builds_access_level
  ci_config_path                         = each.value.ci_config_path
  ci_default_git_depth                   = each.value.ci_default_git_depth
  ci_forward_deployment_enabled          = each.value.ci_forward_deployment_enabled
  ci_restrict_pipeline_cancellation_role = each.value.ci_restrict_pipeline_cancellation_role
  ci_separated_caches                    = each.value.ci_separated_caches
  container_registry_access_level        = each.value.container_registry_access_level
  default_branch                         = each.value.default_branch
  # description                                      = each.value.description
  environments_access_level = each.value.environments_access_level
  # external_authorization_classification_label      = each.value.external_authorization_classification_label
  feature_flags_access_level                       = each.value.feature_flags_access_level
  forked_from_project_id                           = each.value.forked_from_project_id
  group_runners_enabled                            = each.value.group_runners_enabled
  infrastructure_access_level                      = each.value.infrastructure_access_level
  initialize_with_readme                           = each.value.initialize_with_readme
  issues_access_level                              = each.value.issues_access_level
  issues_enabled                                   = each.value.issues_enabled
  issues_template                                  = each.value.issues_template
  keep_latest_artifact                             = each.value.keep_latest_artifact
  lfs_enabled                                      = each.value.lfs_enabled
  merge_commit_template                            = each.value.merge_commit_template
  merge_method                                     = each.value.merge_method
  merge_pipelines_enabled                          = each.value.merge_pipelines_enabled
  merge_requests_access_level                      = each.value.merge_requests_access_level
  merge_requests_enabled                           = each.value.merge_requests_enabled
  merge_requests_template                          = each.value.merge_requests_template
  merge_trains_enabled                             = each.value.merge_trains_enabled
  monitor_access_level                             = each.value.monitor_access_level
  namespace_id                                     = var.namespace_id
  only_allow_merge_if_all_discussions_are_resolved = each.value.only_allow_merge_if_all_discussions_are_resolved
  only_allow_merge_if_pipeline_succeeds            = each.value.only_allow_merge_if_pipeline_succeeds
  packages_enabled                                 = each.value.packages_enabled
  pages_access_level                               = each.value.pages_access_level
  path                                             = each.value.path
  printing_merge_request_link_enabled              = each.value.printing_merge_request_link_enabled
  releases_access_level                            = each.value.releases_access_level
  remove_source_branch_after_merge                 = each.value.remove_source_branch_after_merge
  repository_access_level                          = each.value.repository_access_level
  repository_storage                               = each.value.repository_storage
  request_access_enabled                           = each.value.request_access_enabled
  requirements_access_level                        = each.value.requirements_access_level
  resolve_outdated_diff_discussions                = each.value.resolve_outdated_diff_discussions
  restrict_user_defined_variables                  = each.value.restrict_user_defined_variables
  security_and_compliance_access_level             = each.value.security_and_compliance_access_level
  shared_runners_enabled                           = each.value.shared_runners_enabled
  snippets_access_level                            = each.value.snippets_access_level
  snippets_enabled                                 = each.value.snippets_enabled
  squash_commit_template                           = each.value.squash_commit_template
  squash_option                                    = each.value.squash_option
  suggestion_commit_message                        = each.value.suggestion_commit_message
  tags                                             = each.value.tags
  visibility_level                                 = each.value.visibility_level
  wiki_access_level                                = each.value.wiki_access_level
  wiki_enabled                                     = each.value.wiki_enabled
  push_rules {
    author_email_regex            = each.value.push_rules.author_email_regex
    branch_name_regex             = each.value.push_rules.branch_name_regex
    commit_committer_check        = each.value.push_rules.commit_committer_check
    commit_message_negative_regex = each.value.push_rules.commit_message_negative_regex
    commit_message_regex          = each.value.push_rules.commit_message_regex
    deny_delete_tag               = each.value.push_rules.deny_delete_tag
    file_name_regex               = each.value.push_rules.file_name_regex
    max_file_size                 = each.value.push_rules.max_file_size
    member_check                  = each.value.push_rules.member_check
    prevent_secrets               = each.value.push_rules.prevent_secrets
    reject_unsigned_commits       = each.value.push_rules.reject_unsigned_commits
  }
}

locals {
  variables = flatten([
    for project_key, project in var.projects : [
      for key, values in project.variables : {
        project_key       = project_key
        key               = key
        value             = values.value
        protected         = values.protected
        masked            = values.masked
        environment_scope = values.environment_scope
        variable_type     = values.variable_type
      }
    ]
  ])
  schedules = flatten([
    for project_key, project in var.projects : [
      for key, values in project.schedules : {
        project_key   = project_key
        description   = key
        ref           = values.ref
        cron          = values.cron
        active        = values.active
        cron_timezone = values.cron_timezone
      }
    ]
  ])
  schedules_variables = flatten([
    for project_key, project in var.projects : [
      for schedule_key, schedule_values in project.schedules : [
        for key, values in schedule_values.variables : {
          project_key  = project_key
          schedule_key = schedule_key
          key          = key
          value        = values.value
        }
      ]
    ]
  ])
}

resource "gitlab_branch_protection" "this" {
  for_each = { for key, value in var.projects : key => value if value.branch_protection.enabled }

  project                      = gitlab_project.this[each.key].id
  branch                       = gitlab_project.this[each.key].default_branch
  allow_force_push             = each.value.branch_protection.allow_force_push
  push_access_level            = each.value.branch_protection.push_access_level
  merge_access_level           = each.value.branch_protection.merge_access_level
  code_owner_approval_required = each.value.branch_protection.code_owner_approval_required

  dynamic "allowed_to_push" {
    for_each = each.value.branch_protection.users_allowed_to_push
    content {
      user_id = allowed_to_push.value
    }
  }

}

resource "gitlab_project_approval_rule" "this" {
  for_each = { for key, value in var.projects : key => value if value.approval_rule.enabled }

  project              = gitlab_project.this[each.key].id
  name                 = each.value.approval_rule.name
  approvals_required   = each.value.approval_rule.approvals_required
  group_ids            = var.approval_rule_group_ids
  protected_branch_ids = [gitlab_branch_protection.this[each.key].branch_protection_id]
}

resource "gitlab_project_level_mr_approvals" "this" {
  for_each = { for key, value in var.projects : key => value if value.level_mr_approvals.enabled }

  project                                        = gitlab_project.this[each.key].id
  reset_approvals_on_push                        = each.value.level_mr_approvals.reset_approvals_on_push
  disable_overriding_approvers_per_merge_request = each.value.level_mr_approvals.disable_overriding_approvers_per_merge_request
  merge_requests_author_approval                 = each.value.level_mr_approvals.merge_requests_author_approval
  merge_requests_disable_committers_approval     = each.value.level_mr_approvals.merge_requests_disable_committers_approval
}

resource "gitlab_project_variable" "variable" {
  for_each = {
    for v in local.variables : "${v.project_key}.${v.key}" => v
  }

  project           = gitlab_project.this[each.value.project_key].id
  key               = each.value.key
  value             = each.value.value
  protected         = each.value.protected
  masked            = each.value.masked
  environment_scope = each.value.environment_scope
}

resource "gitlab_pipeline_schedule" "schedule" {
  for_each = {
    for s in local.schedules : "${s.project_key}.${s.description}" => s
  }

  project       = gitlab_project.this[each.value.project_key].id
  description   = each.value.description
  ref           = each.value.ref
  cron          = each.value.cron
  active        = each.value.active
  cron_timezone = each.value.cron_timezone
}


resource "gitlab_pipeline_schedule_variable" "variable" {
  for_each = {
    for sv in local.schedules_variables : "${sv.project_key}.${sv.schedule_key}.${sv.key}" => sv
  }

  project              = gitlab_project.this[each.value.project_key].id
  pipeline_schedule_id = gitlab_pipeline_schedule.schedule["${each.value.project_key}.${each.value.schedule_key}"].pipeline_schedule_id
  key                  = each.value.key
  value                = each.value.value
}